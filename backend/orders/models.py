from django.db import models


class Order(models.Model):
    order_id = models.CharField(max_length=5, primary_key=True)
    due_date = models.DateField(null=False)
    completion_date = models.DateField()
    created_date = models.DateField(null=False)

    def __str__(self):
        return self.order_id


class OrderStatus(models.Model):
    order_status_id = models.CharField(max_length=20)


class OrderItem(models.Model):
    order_item_id = models.CharField(max_length=6, primary_key=True)
    fk_order_id = models.ForeignKey(Order)
    fk_product_id = models.ForeignKey(Product)
    quantity = models.IntegerField(max_length=10)
    fk_order_status_id = models.ForeignKey(OrderStatus)
    fk_inspection_id = models.ForeignKey(Inspection, null=True)